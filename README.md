# Bulk move issues

Bulk move a number of issues from one project to another. Write a report of which issues have been moved.

## Features

- Bulk move all issues in one project to another
- Optionally limit moved issues by defining labels of issues to move
- Don't move issues if source project labels are not available in target project to avoid data loss of labels
- Write a log file of moved issue iids

## Limitations

- The script will make sure you don't lose labels on the issue by checking that the target project contains the defined filter labels of the source project. To move issues without loss of labels, use group labels available to both projects.
- The script will **not** check milestones, iterations or epics, which may also be project specific or group specific and won't be moved. **Some data loss in these fields is possible when using this script**
- As issues are moved to another project, thus losing ther IID (their identifier within the project, also used for API get calls) and getting a new one, **moving them back specifically is impossible**. Only all issues in the target project can be moved elsewhere, which may include additional issues that were in the target project before. 
- Be aware of this restriction and make sure to check carefully in order to avoid data loss.

## Usage

You can run this via GitLab CI. Clone or fork this repo, set it up in your group and modify the project IDs in the `.gitlab-ci.yml` file.

Make sure to configure an API token named `GIT_TOKEN` with the appropriate permissions in the `Settings->CI/CD->Variables` interface.

You can use the `CI/CD->Pipeline->Run Pipeline` user interface to provide the project IDs and labels via a handy UI.

## Script and Parameters

`python3 bulk_move_issues.py $GIT_TOKEN $SOURCE_PROJECT_ID $TARGET_PROJECT_ID --labels $LABELS --gitlab $GITLAB_INSTANCE`

- `GIT_TOKEN`: API Token able to read/write issues in source and target project
- `SOURCE_PROJECT_ID`: Project ID of the source project to move issues from
- `TARGET_PROJECT_ID`: Project ID of the target project to move issues to
- `--labels $LABELS`: optional comma-separated list of labels that define issues to be moved. Issues must have all labels stated in the list (AND conjunction)
- `--gitlab $GITLAB_INSTANCE`: optional GitLab instance URL, default to gitlab.com

## DISCLAIMER

This script is provided for educational purposes. It is **not supported by GitLab**. However, you can create an issue if you need help or propose a Merge Request. Be aware that, once issues are moved to a project and data, like milestones, has been lost, there is no way to roll this back.

